package ru.buglakov.skypemusicbot.skype;

/**
 * @author: rd
 * @since: 12/1/13 7:03 PM
 */
public class Message {
    private final Skype skype;
    private final String id;
    private final String body;
    private String sender;
    private boolean isReceivingSender = false;


    protected Message(Skype skype, String msgId, String body){
        this.skype = skype;
        id = msgId;
        this.body = body;
    }

    public String getSender(){
        if(sender==null){
            if(isReceivingSender==false){
                isReceivingSender=true;
                skype.requestSender(this);
            }
            synchronized (this){
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    System.out.println();
                }
            }
        }
        return sender;
    }

    protected void setSender(String sender){
        this.sender = sender;
        isReceivingSender=false;
        synchronized (this){
            this.notify();
        }
    }

    protected String getId(){
        return id;
    }

    public String getBody(){
        return body;
    }

    @Override
    public String toString(){
        return body;
    }
}
