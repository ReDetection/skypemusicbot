package ru.buglakov.skypemusicbot.skype;

import com.skype.connector.*;
import ru.buglakov.skypemusicbot.util.DaemonThread;

import java.util.Hashtable;
import java.util.Map;

/**
 * @author: rd
 * @since: 09.12.12 19:53
 *
 */
public class Skype extends Thread implements ConnectorListener {

    private static final String CHATMESSAGE_OBJECT = "CHATMESSAGE";
    private static final String CHATNAME_PROPERTY = "CHATNAME";
    private static final String BODY_PROPERTY = "BODY";
    private static final String STATUS_PROPERTY = "STATUS";
    private static final String STATUS_RECEIVED_VALUE = "RECEIVED";
    private static final String GET_CHATMESSAGE_BODY_FORMAT = "GET " + CHATMESSAGE_OBJECT + " %s " + BODY_PROPERTY;
    private static final String GET_CHATMESSAGE_CHATNAME_FORMAT = "GET " + CHATMESSAGE_OBJECT + " %s " + CHATNAME_PROPERTY;

    private boolean debug = false;
    private Connector connector;
    private MessageListener listener;
    private Map<String,Message> waitingMessages = new Hashtable<String, Message>();


    public Skype(MessageListener listener){
        this.listener = listener;

    }

    @Override
    public void run() {
        try {
            connector = Connector.getInstance();
            connector.addConnectorListener(this);
            connector.setDebug(debug);
        } catch (ConnectorException e) {e.printStackTrace(); }

        new DaemonThread().start();
    }

    private void request(String s){
        if(debug)print("requesting: "+s);
        try {
            connector.executeWithoutTimeout(s, "");
        } catch (ConnectorException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void fireMessage(String msgId, String str){
        if (listener!=null){
            listener.messageReceived(new Message(this,msgId,str));
        }
    }

    @Override
    public void messageReceived(ConnectorMessageEvent event) {
        String m = event.getMessage();
        String[] parts = m.split(" ", 5);

        print("recv: " + m);
        if(CHATMESSAGE_OBJECT.equals(parts[0])){
            if(STATUS_PROPERTY.equals(parts[2]) && STATUS_RECEIVED_VALUE.equals(parts[3])){
                String messageid = parts[1];
                request(String.format(GET_CHATMESSAGE_BODY_FORMAT,messageid));
            }else if(BODY_PROPERTY.equals(parts[2])){
                int len = 0;
                for(int i=0;i<3;i++){
                    len+=parts[i].length();
                    len++; //пробел
                }
                fireMessage(parts[1], m.substring(len));
            }else if(CHATNAME_PROPERTY.equals(parts[2])){
                Message msg = waitingMessages.get(parts[1]);
                if(msg!=null){
                    waitingMessages.remove(parts[1]);
                    msg.setSender(parts[3]);
                }
            }
        }
    }

    @Override
    public void messageSent(ConnectorMessageEvent event) {
        print("sent: " + event.getMessage());
    }

    @Override
    public void statusChanged(ConnectorStatusEvent event) {
        print("status: " + event.getStatus().toString());
        if("NOT_RUNNING".equals(event.getStatus().toString())){
            System.exit(1);
        }
    }

    private void print(String s){
        if( debug){
            System.out.println(s);
            System.out.flush();
        }
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;

        if(connector!=null){
            try {
                connector.setDebug(debug);
            } catch (ConnectorException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void sendMessage(String chat, String message){
        request("CHATMESSAGE " + chat + " " + message);
    }

    public void requestSender(Message message) {
        waitingMessages.put(message.getId(),message);
        request(String.format(GET_CHATMESSAGE_CHATNAME_FORMAT, message.getId()));
    }
}
