package ru.buglakov.skypemusicbot.skype;

/**
 * @author: rd
 * @since: 09.12.12 20:09
 *
 */
public interface MessageListener {
    public void messageReceived(Message msg);
}
