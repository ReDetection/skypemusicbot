package ru.buglakov.skypemusicbot.player;

import java.io.IOException;

/**
 * @author: rd
 * @since: 24/12/12 8:15 AM
 *
 */
public class MusicPlayer {

    private Runtime runtime = Runtime.getRuntime();
    private String[] exec2 = new String[2];
    private String[] exec3 = new String[3];

    public MusicPlayer(){
        exec2[0] = "/usr/local/bin/mpc";
        exec3[0] = "/usr/local/bin/mpc";
    }

    public void play(String url){
        try {
            execCommand("clear");
            execCommand("add",url);
            execCommand("stop");
            execCommand("play");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void play(String urls[]){
        try {
            execCommand("clear");
            for(String url: urls){
                execCommand("add",url);
            }
            execCommand("stop");
            execCommand("play");
            execCommand("repeat","on");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void execCommand(String arg) throws IOException {
        exec2[1] = arg;
        runtime.exec(exec2);
    }

    private void execCommand(String arg1, String arg2) throws IOException {
        exec3[1] = arg1;
        exec3[2] = arg2;
        runtime.exec(exec3);
    }





}
