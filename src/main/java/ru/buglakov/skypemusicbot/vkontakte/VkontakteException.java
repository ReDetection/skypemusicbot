package ru.buglakov.skypemusicbot.vkontakte;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author: rd
 * @since: 4/1/13 11:05 AM
 */
public class VkontakteException extends Exception{

    private int code;

    public VkontakteException(String msg){
        super(msg);
        code = -1;
    }

    public VkontakteException(JSONObject error) throws JSONException {
        super(error.getString("error_msg"));
        code = error.getInt("error_code");

    }

    public int getCode() {
        return code;
    }
}
