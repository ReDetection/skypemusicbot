package ru.buglakov.skypemusicbot.vkontakte;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @author: rd
 * @since: 09.12.12 22:54
 *
 */
public class Vkontakte {

    private static final String BASE_URL = "https://api.vk.com/method/";
    private static final String AUTHORIZE_URL  = "https://oauth.vk.com/authorize?client_id=2810295&scope=8&display=page&response_type=token&redirect_uri=http%3A%2F%2Foauth.vk.com%2Fblank.html";

    private HttpClient httpClient = new DefaultHttpClient();
    private String token = null;

    public String[] searchAudio(String query, int count) throws VkontakteException {
        if(token==null){
            throw new VkontakteException("No token");
        }
        JSONObject json = null;
        try {
            json = action("audio.search", "q=" + URLEncoder.encode(query, "UTF-8") + "&count=" + count);
            JSONArray arr = json.getJSONArray("response");
            String[] result = new String[arr.length()-1];
            for(int i=result.length;i>0;){
                json = arr.getJSONObject(i);
                i--;
                result[i] = json.getString("url");
            }
            return result;
        } catch (UnsupportedEncodingException e) {
        } catch (JSONException e) {
            try {
                throw new VkontakteException(json.getJSONObject("error"));
            } catch (JSONException e1) {
                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return null;
    }


    private JSONObject action(String method, String parameters){
        try {
            HttpGet get = new HttpGet(BASE_URL + method + "?" + parameters + "&access_token=" + token);
            HttpResponse response = httpClient.execute(get);
            String body = IOUtils.toString(response.getEntity().getContent());
            return new JSONObject(body);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAuthorizeURL(){
        return AUTHORIZE_URL;
    }
}
