package ru.buglakov.skypemusicbot;

import ru.buglakov.skypemusicbot.player.MusicPlayer;
import ru.buglakov.skypemusicbot.skype.Message;
import ru.buglakov.skypemusicbot.skype.MessageListener;
import ru.buglakov.skypemusicbot.skype.Skype;
import ru.buglakov.skypemusicbot.vkontakte.Vkontakte;
import ru.buglakov.skypemusicbot.vkontakte.VkontakteException;

/**
 * @author: rd
 * @since: 03.12.12 20:18
 *
 */
public class Main implements MessageListener {

    private static final String MUSIC_COMMAND = "музыка";
    private static final String TOKEN_COMMAND = "токен";

    private Skype skype;
    private Vkontakte vk;
    private MusicPlayer player;

    public static void main(String args[]) {
        new Main();
    }

    public Main(){
        skype = new Skype(this);
        vk = new Vkontakte();
        player = new MusicPlayer();

//        skype.setDebug(true);
        skype.start();
    }

    private void sendToSkype(String buddy, String msg) {
        print("шлем " + buddy + " сообщение: " + msg);
        skype.sendMessage(buddy, msg);
    }

    @Override
    public void messageReceived(Message message) {
        print("пришло из API: " + message);
        String[] parts = message.getBody().split(" ",2);
        if(MUSIC_COMMAND.equalsIgnoreCase(parts[0])){
            if(parts[1].startsWith("http")==false){
                String[] urls = vkGetTrakcs(message,parts[1], 20);
                if(urls==null)return;
                print("найдены треки " + urls);
                player.play(urls);
            }else{
                player.play(parts[1]);
            }
        }
        if(TOKEN_COMMAND.equalsIgnoreCase(parts[0])){
            vk.setToken(parts[1]);
        }
    }

    private String[] vkGetTrakcs(Message message, String request, int count){
        try {
            return vk.searchAudio(request,count);
        } catch (VkontakteException e) {
            processVkException(message,e);
            return null;
        }
    }

    private void processVkException(Message message, VkontakteException e){
        if(e.getCode()==5 || e.getCode()==-1){
            sendToSkype(message.getSender(),"Вконтакте токен истек, скопируйте это в браузер, разрешите доступ, полученный токен отправьте в чат, но напишите слово токен перед ним! " + vk.getAuthorizeURL() + "\nRoot cause: " + e.getMessage());
        }else{
            sendToSkype(message.getSender(),"Ошибка вконтакте, код " + e.getCode() + ":" + e.getMessage());
        }
    }


    private void print(String s){
        System.out.println(s);
        System.out.flush();
    }
}
